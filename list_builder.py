import os, sys
import requests

install_dir = os.path.dirname(os.path.realpath(__file__))

blacklist_dir = os.path.join(install_dir, "blacklists/")
blacklist_urls = os.path.join(blacklist_dir, "source_urls")

whitelist_dir = os.path.join(install_dir, "whitelists/")
whitelist_urls = os.path.join(whitelist_dir, "source_urls")


# step1: put "dirty" urls into list
# step2: sort urls alphabetically
# step3: remove duplicates
# step4: remove ips
# step5: save urls into new "clean" file
# step6: download "clean" url file 

class OrganizeList():
    def __init__(self, the_list):
        self.dirty_list = the_list

    def alphabetical(self):
        self.dirty_list = sorted(self.dirty_list)
        return self.dirty_list

    def remove_duplicates(self):
        self.dirty_list = list(dict.fromkeys(self.dirty_list))
        return self.dirty_list

    def remove_ips(self):
        pass

    def do_all(self):
        send_to_alph = self.alphabetical()
        send_to_remove_dupes = self.remove_duplicates()
        
        return self.dirty_list

def setup_list(url_file):
    dirty_list = []

    with open(url_file) as f:
        for line in f:
            dirty_list.append(line.strip('\n\r'))

    return dirty_list

def download_list(the_list, file_name):
    pop_list = []
    dirty_dl_list = []

    if file_name == "whitelist":
        file_name == "whitelist_domains"
    elif file_name == "blacklist":
        file_name == "blacklist_domains"
    
    # remove file in preparation for new list
    if os.path.exists(file_name):
        os.remove(file_name)

    
    for url in the_list:
        try:
            r = requests.get(url, timeout=5, stream=True)

            if r.status_code == 200:
                for line in r.text:
                    if not line.startswith('#'):
                        dirty_dl_list.append(line)
            else:
                pop_list.append(url)
        except requests.exceptions.ConnectionError as connect_error:
            pop_list.append(url)
        except requests.exceptions.ReadTimeout as read_timeout:
            pop_list.append(url)

    # broken right now, returning strange text
    #cleaned_dl_list = OrganizeList(dirty_dl_list).remove_duplicates()

    #with open(file_name, "a") as f:
    #    for line in cleaned_dl_list:
    #        f.write(line)

    with open(file_name, "a") as f:
        for line in dirty_dl_list:
            f.write(line)

    # used to remove bad urls
    #print(*pop_list, sep='\n')

def main():
    dirty_b_list = setup_list(blacklist_urls)
    #print(*dirty_b_list, sep='\n')

    organized_b_list = OrganizeList(dirty_b_list).do_all()
    #print(*organized_b_list, sep='\n')

    download_list(organized_b_list, "blacklist")

    dirty_w_list = setup_list(whitelist_urls)
    #print(*dirty_w_list, sep='\n')
    
    organized_w_list = OrganizeList(dirty_w_list).do_all()
    #print(*organized_w_list, sep='\n')

    download_list(organized_w_list, "whitelist")

if __name__ == "__main__":
   main()